var program = require('commander');
var cluster = require('cluster');

program
  .version('1.0')
  .usage('<options>')
  .option('-b, --bind <address>', 'Path or address:port to bind to [localhost:8000]')
  .option('-c, --count [number]', 'Number of processes to fork. [1]', parseInt)
  .parse(process.argv);

  program.bind = program.bind || 'localhost:8000';
  program.count = program.count || 1;

function startMaster() {

  console.log('Starting %s on %s...', program.name(), program.bind);

  var count = program.count;
  while (count--) {
    cluster.fork();
  }

  return cluster.on('exit', function(worker, code, signal) {
    console.log('worker ' + worker.process.pid + ' died. restarting...');
    cluster.fork();
  });
}

function startChild() {
  var server = require('./server.js');
  var bind = program.bind.split(':');
  var port = bind[1];
  var host = bind[0];
  return server.listen(port, host);
}


module.exports = cluster.isMaster ? startMaster() : startChild();