var Http = require('http');
var HttpProxy = require('http-proxy');


var X_PROXY_TO = 'x-proxy-to';
var CONNECTION = 'Connection';
var KEEP_ALIVE = 'Keep-Alive';

var _connections = {};

function createProxy(url) {
  return _connections[url] || (_connections[url] = HttpProxy.createProxyServer({
      target: url,
      xfwd: true
    })
    .on('proxyReq', function(proxyReq, req, res, options) {
      proxyReq.setHeader(CONNECTION, KEEP_ALIVE);
    })
    .on('error', function(err, req, res) {
      res.statusCode = 502;
      res.end();
    })
  );
}

module.exports = Http.createServer(function(req, res) {
  var proxy;
  var url = req.headers[X_PROXY_TO];
  if (url) {
    proxy = createProxy(url);
    proxy.web(req, res);
  }
  else {
    res.statusCode = 400;
    res.end();
  }
});

